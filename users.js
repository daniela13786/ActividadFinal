// controllers/userController.js
let users = [
  { id: 1, name: 'John' },
  { id: 2, name: 'Jane' }
];

// Obtener todos los usuarios
exports.getUsers = (req, res) => {
  res.json(users);
};

// Obtener un usuario por su ID
exports.getUserById = (req, res) => {
  const userId = parseInt(req.params.id);
  const user = users.find(user => user.id === userId);
  if (user) {
    res.json(user);
  } else {
    res.status(404).send('Usuario no encontrado');
  }
};

// Agregar un nuevo usuario
exports.createUser = (req, res) => {
  const newUser = req.body;
  users.push(newUser);
  res.status(201).json(newUser);
};

// Actualizar un usuario por su ID
exports.updateUserById = (req, res) => {
  const userId = parseInt(req.params.id);
  const updateUser = req.body;
  const index = users.findIndex(user => user.id === userId);
  if (index !== -1) {
    users[index] = { ...users[index], ...updateUser };
    res.json(users[index]);
  } else {
    res.status(404).send('Usuario no encontrado');
  }
};

// Eliminar un usuario por su ID
exports.deleteUserById = (req, res) => {
  const userId = parseInt(req.params.id);
  const index = users.findIndex(user => user.id === userId);
  if (index !== -1) {
    users.splice(index, 1);
    res.status(204).send(); // No Content
  } else {
    res.status(404).send('Usuario no encontrado');
  }
};
