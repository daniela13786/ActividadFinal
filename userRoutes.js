// routes/userRoutes.js
const express = require('express');
const router = express.Router();
const user = require('../Actividad6/users');

// Rutas CRUD para usuarios
router.get('/', user.getUsers);
router.get('/:id', user.getUserById);
router.post('/', user.createUser);
router.put('/:id', user.updateUserById);
router.delete('/:id', user.deleteUserById);

module.exports = router;
