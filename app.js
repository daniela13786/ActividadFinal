const express = require('express');
const app = express();
const cors = require('cors'); 
const port = 3000;

// Middleware para parsear JSON
app.use(express.json());
app.use(cors());
// Importar las rutas de usuarios
const userRoutes = require('../Actividad6/userRoutes');

// Usar las rutas de usuarios en la aplicación
app.use('/users', userRoutes);

app.listen(port, () => {
  console.log(`La aplicación está escuchando en http://localhost:${port}`);
});
